QUnit.test('Testing Addition function with four sets of inputs', function (assert) {
    assert.throws(function () { finalsum(); }, new Error("only numbers are allowed"), 'Passing in array correctly raises an Error');
    assert.strictEqual(finalsum(2,4,2,8), 16, 'All positive numbers');
    assert.strictEqual(finalsum(3,-16,-3,0), -16, 'Positive and negative numbers');
    assert.strictEqual(finalsum(-4,-8,-4,-6), -22, 'All are negative numbers');
});
